using ErrorHandling.Task1.ThirdParty;
using System;

namespace ErrorHandling.Task1
{
    public class UserReportController
    {

        private UserReportBuilder userReportBuilder;

        public string GetUserTotalOrderAmountView(string userId, IModel model)
        {
            try
            {
                string totalMessage = GetUserTotalMessage(userId);
                model.AddAttribute("userTotalMessage", totalMessage);
                return "userTotal";
            }
            catch (NullReferenceException)
            {
                return "technicalError";
            }
            
        }

        private string GetUserTotalMessage(string userId)
        {

            try
            {
                double amount = userReportBuilder.GetUserTotalOrderAmount(userId);
                return "User Total: " + amount + "$";
            }
            catch (NoUserFoundException)
            {
                return "WARNING: User ID doesn't exist.";
            }
            catch (EmptyListException)
            {
                return "WARNING: User have no submitted orders.";
            }
            catch (InvalidOrderTotalException)
            {
                return "ERROR: Wrong order amount.";
            }
        }


        public UserReportBuilder GetUserReportBuilder()
        {
            return userReportBuilder;
        }

        public void SetUserReportBuilder(UserReportBuilder userReportBuilder)
        {
            this.userReportBuilder = userReportBuilder;
        }
    }
}
