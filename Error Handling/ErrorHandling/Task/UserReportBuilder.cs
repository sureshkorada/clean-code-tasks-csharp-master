using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using ErrorHandling.Task1.ThirdParty;

namespace ErrorHandling.Task1
{
    public class UserReportBuilder
    {

        private IUserDao userDao;

        public double GetUserTotalOrderAmount(string userId)
        {
            if (userDao == null)
                throw new NullReferenceException();

            IUser user = userDao.GetUser(userId);
            if (user == null)
                throw new NoUserFoundException();

            IList<IOrder> orders = user.GetAllOrders();

            if (orders.Count == 0)
                throw new EmptyListException();

            double sum = 0.0;
            foreach (IOrder order in orders) {

                if (order.IsSubmitted()) {
                    double total = order.Total();
                    if (total < 0)
                        throw new InvalidOrderTotalException();
                    sum += total;
                }
            }

            return sum;
        }


        public IUserDao GetUserDao()
        {
            return userDao;
        }

        public void SetUserDao(IUserDao userDao)
        {
            this.userDao = userDao;
        }
    }

    [Serializable]
    internal class InvalidOrderTotalException : Exception
    {
        public InvalidOrderTotalException()
        {
        }

        public InvalidOrderTotalException(string message) : base(message)
        {
        }

        public InvalidOrderTotalException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected InvalidOrderTotalException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }

    [Serializable]
    internal class EmptyListException : Exception
    {
        public EmptyListException()
        {
        }

        public EmptyListException(string message) : base(message)
        {
        }

        public EmptyListException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected EmptyListException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }

    [Serializable]
    internal class NoUserFoundException : Exception
    {
        public NoUserFoundException()
        {
        }

        public NoUserFoundException(string message) : base(message)
        {
        }

        public NoUserFoundException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected NoUserFoundException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
