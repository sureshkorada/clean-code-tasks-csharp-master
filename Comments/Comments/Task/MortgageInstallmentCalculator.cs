﻿using System;
using Comments.Task1.ThirdParty;

namespace Comments.Task1
{
    public static class MortgageInstallmentCalculator
    {
        
        public static double CalculateMonthlyPayment(int principalAmount, int termInYears, double rateOfInterest)
        {
            
            if (principalAmount < 0 || termInYears < 0 || rateOfInterest < 0)
            {
                throw new InvalidInputException("Negative values are not allowed");
            }

            // convert interest rate into a decimal - eg. 6.5% = 0.065
            rateOfInterest /= 100;

            int termInMonths = termInYears * 12;

            if (rateOfInterest == 0)
                return (double) principalAmount / termInMonths;

            double monthlyInterest = rateOfInterest / 12;
            double monthlyPayment = principalAmount * monthlyInterest / (1 - Math.Pow(1 + monthlyInterest, -termInMonths));

            return monthlyPayment;
        }
    }
}
