﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Microsoft.Extensions.FileProviders;
using Naming.Task5.ThirdParty;

namespace Naming.Task5
{
    public class FileManager
    {
        private static readonly string[] ImageTypes = {"jpg", "png"};
        private static readonly string[] DocumentTypes = {"pdf", "doc"};

        private const string BaseNameSpace = "Naming.Resources";
        private readonly IFileProvider FileProvider = new EmbeddedFileProvider(Assembly.GetExecutingAssembly(), BaseNameSpace);

        public IFileInfo RetrieveFile(string fileName)
        {
            ValidateFileType(fileName);
            return FileProvider.GetFileInfo(fileName);
        }

        #region RetrieveFile

        private static void ValidateFileType(string fileName)
        {
            if (IsInValidFileType(fileName))
            {
                throw new InvalidFileTypeException();
            }
        }

        private static bool IsInValidFileType(string fileName)
        {
            return IsInValidImage(fileName) && IsInValidDocument(fileName);
        }

        private static bool IsInValidImage(string fileName)
        {
            var imageExtensionsPredicate = new FileExtensionPredicate(ImageTypes);
            return !imageExtensionsPredicate.Test(fileName);
        }

        private static bool IsInValidDocument(string fileName)
        {
            var documentExtensionsPredicate = new FileExtensionPredicate(DocumentTypes);
            return !documentExtensionsPredicate.Test(fileName);
        }

        #endregion

        public List<string> ListAllImages()
        {
            return Files(ImageTypes);
        }

        public List<string> ListAllDocuments()
        {
            return Files(DocumentTypes);
        }

        private List<string> Files(string[] allowedExtensions)
        {
            var predicate = new FileExtensionPredicate(allowedExtensions);
            return FileProvider.GetDirectoryContents(string.Empty)
                .Select(f => f.Name)
                .Where(predicate.Test)
                .ToList();
        }
    }
}
