﻿using System.Linq;
using Naming.Task5.ThirdParty;

namespace Naming.Task5
{
    public class FileExtensionPredicate : IPredicate<string>
    {
        private readonly string[] extensions;

        public FileExtensionPredicate(string[] extensions)
        {
            this.extensions = extensions;
        }

        public bool Test(string fileName)
        {
            fileName = fileName.ToLower();
            return extensions.Any(fileName.EndsWith);
        }
    }
}
