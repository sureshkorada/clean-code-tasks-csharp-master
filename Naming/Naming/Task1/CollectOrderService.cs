﻿using Naming.Task1.ThirdParty;

namespace Naming.Task1
{
    public class CollectOrderService : IOrderService
    {
        private const int InfoLevel = 4;
        private const int CriticalLevel = 1;
        private readonly ICollectionService collectionService;
        private readonly INotificationManager notificationManager;

        public CollectOrderService(ICollectionService collectionService, INotificationManager notificationManager)
        {
            this.collectionService = collectionService;
            this.notificationManager = notificationManager;
        }

        public void SubmitOrder(IOrder order)
        {
            if (collectionService.IsEligibleForCollect(order))
            {
                notificationManager.NotifyCustomer(Message.ReadyForCollect, InfoLevel); 
            }
            else
            {
                notificationManager.NotifyCustomer(Message.ImpossibleToCollect, CriticalLevel); 
            }
        }
    }
}
