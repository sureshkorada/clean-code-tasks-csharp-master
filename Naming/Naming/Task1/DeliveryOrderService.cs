﻿using System.Collections.Generic;
using Naming.Task1.ThirdParty;

namespace Naming.Task1
{
    public class DeliveryOrderService : IOrderService
    {
        private readonly IDeliveryService deliveryService;
        private readonly IOrderFulfilmentService orderFulfilmentService;

        public DeliveryOrderService(IDeliveryService deliveryService, IOrderFulfilmentService orderFulfilmentService)
        {
            this.deliveryService = deliveryService;
            this.orderFulfilmentService = orderFulfilmentService;
        }

        public void SubmitOrder(IOrder order)
        {
            if (deliveryService.IsDeliverable())
            {
                IList<IProduct> products = order.GetProducts();
                orderFulfilmentService.FulfilProducts(products);
            }
            else
            {
                throw new NotDeliverableOrderException();
            }
        }
    }
}
