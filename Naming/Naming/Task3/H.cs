﻿using System;

namespace Naming.Task3
{
    public class H
    {
        // print some Harshad numbers
        private static void Main(string[] args)
        {
            const long Limit = 1000; // limit the seq of Harshad numbers
            for (int number = 1; number <= Limit; number++)
            {
                if (number % SumOfDigits(number) == 0)
                {
                    Console.WriteLine(number);
                }
            }

            Console.Write("Press key...");
            Console.ReadKey();
        }

        private static int SumOfDigits(int num)
        {
            int sumOfDigits = 0;
            while (num != 0)
            {
                sumOfDigits += num % 10;
                num = num / 10;
            }
            return sumOfDigits;
        }
    }
}
