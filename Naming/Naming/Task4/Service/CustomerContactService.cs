﻿using Naming.Task4.ThirdParty;

namespace Naming.Task4.Service
{
    public class CustomerContactService : ICustomerContactService
    {
        private readonly ICustomerContactRepository customerContactRepository;

        public CustomerContactService(ICustomerContactRepository customerContactRepository)
        {
            this.customerContactRepository = customerContactRepository;
        }

        public CustomerContact FindByCustomerId(float customerId)
        {
            return customerContactRepository.FindById(customerId);
        }

        public void Update(CustomerContact customerContact)
        {
            customerContactRepository.Update(customerContact);
        }
    }
}
