﻿using System;

namespace Naming.Task2
{
    public class User
    {
        private readonly DateTime dateOfBirth;

        private readonly string name;

        private readonly bool admin;

        private readonly User[] subordinateArray;

        private readonly int rating;

        public User(string name)
        {
            this.name = name;
        }

        public override string ToString()
        {
            return "User [dBirth=" + dateOfBirth + ", sName=" + name + ", bAdmin=" + admin + ", subordinateArray="
                   + string.Join<User>(", ", subordinateArray) + ", iRating=" + rating + "]";
        }
    }
}
