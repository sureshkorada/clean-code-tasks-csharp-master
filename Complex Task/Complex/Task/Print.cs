﻿using System;
using System.Collections.Generic;
using Complex.Task1.ThirdParty;

namespace Complex.Task1
{
    public class Print : ICommand
    {
        private readonly IDatabaseManager manager;
        private readonly IView view;
        private string tableName;
        private const string COLUMN_SEPERATOR = "║";
        private const string ROW_SEPERATOR = "═";
        private const string TOP_LEFT_CORNER = "╔";
        private const string TOP_RIGHT_CORNER = "╗";
        private const string BOTTOM_LEFT_CORNER = "╚";
        private const string BOTTOM_RIGHT_CORNER = "╝";
        private const string T_FIGURE = "╦";
        private const string INVERTED_T_FIGURE = "╩";
        private const string LEFT_ROTATE_T_FIGURE = "╠";
        private const string RIGHT_ROTATE_T_FIGURE = "╣";
        private const string CROSS_FIGURE = "╬";
        private const string NEW_LINE = "\n";
        private const string BLANK_SPACE = " ";
        private const char COMMAND_SEPERATOR = ' ';
        private const string EMPTY_STRING = "";
        


        public Print(IView view, IDatabaseManager manager)
        {
            this.view = view;
            this.manager = manager;
        }
        public bool CanProcess(string command)
        {
            return command.StartsWith("print ");
        }
        public void Process(string command)
        {
            var commands = command.Split(COMMAND_SEPERATOR);
            if (commands.Length != 2)
                throw new ArgumentException(
                    "incorrect number of parameters. Expected 1, but is " + (commands.Length - 1));
            tableName = commands[1];
            var data = manager.GetTableData(tableName);
            view.Write(GetTableString(data));
        }


        private string GetTableString(IList<IDataSet> data)
        {
            var maxColumnSize = GetMaxColumnSize(data);
            if (maxColumnSize == 0)
                return GetEmptyTable(tableName);
            else
                return GetTableHeader(data) + GetTableBody(data);
        }
        private string GetEmptyTable(string tableName)
        {
            var emptyTableText = COLUMN_SEPERATOR+" Table '" + tableName + "' is empty or does not exist "+COLUMN_SEPERATOR;
            int columnCount = 1;
            int columnSize = emptyTableText.Length - 2;

            var result = EMPTY_STRING;
            result += GetTopRowBorder(columnSize, columnCount);
            result += emptyTableText + NEW_LINE;
            result += GetBottomRowBorder(columnSize, columnCount);
            return result;
        }
        private string GetTableHeader(IList<IDataSet> dataSets)
        {
            var maxColumnSize = GetMaxColumnSize(dataSets);
            maxColumnSize = IncrementMaxColumnSize(maxColumnSize);
            var columnCount = GetColumnCount(dataSets);
            var columnNames = dataSets[0].GetColumnNames();

            var result = EMPTY_STRING;
            result += GetTopRowBorder(maxColumnSize,  columnCount);
            foreach (var columnName in columnNames)
            {
                result += PopulateColumnContent(columnName, maxColumnSize);
            }
            result += COLUMN_SEPERATOR + NEW_LINE;

            if (IsNotEmpty(dataSets))
            {
                result += GetSectionSeperator(maxColumnSize, columnCount);
            }
            else
            {
                result += GetBottomRowBorder(maxColumnSize, columnCount);
            }
            return result;
        }
        private string GetTableBody(IList<IDataSet> dataSets)
        {
            var rowCount = dataSets.Count;
            var columnCount = GetColumnCount(dataSets);

            var maxColumnSize = GetMaxColumnSize(dataSets);
            maxColumnSize = IncrementMaxColumnSize(maxColumnSize);

            var result = EMPTY_STRING;
            for (var row = 0; row < rowCount; row++)
            {
                var values = dataSets[row].GetValues();
                foreach (var columnValue in values)
                {
                    result += PopulateColumnContent(columnValue.ToString(), maxColumnSize);
                }
                result += COLUMN_SEPERATOR + NEW_LINE;
                if (IsLastRow(rowCount, row))
                {
                    result += GetSectionSeperator(maxColumnSize, columnCount);
                }
            }

            result += GetBottomRowBorder(maxColumnSize, columnCount);
            return result;
        }


        private string PopulateColumnContent(string columnText, int maxColumnSize)
        {
            var result = EMPTY_STRING;
            var columnDataLength = columnText.Length;
            var leftPaddingLength = GetPaddingLength(columnDataLength, maxColumnSize);
            var rightPaddingLength = leftPaddingLength;

            if (IsNotEven(columnDataLength))
            {
                rightPaddingLength++;
            }

            result += COLUMN_SEPERATOR;
            result += AddPadding(leftPaddingLength);
            result += columnText;
            result += AddPadding(rightPaddingLength);

            return result;
        }
        private int GetPaddingLength(int columnDataLength,int maxColumnSize)
        {
            return (maxColumnSize - columnDataLength) / 2;
        }
        private static string AddPadding( int paddingLength)
        {
            string result = EMPTY_STRING;
            for (var j = 0; j < paddingLength; j++)
                result += BLANK_SPACE;
            return result;
        }


        private int GetColumnCount(IList<IDataSet> dataSets)
        {
            var result = 0;
            if (IsNotEmpty(dataSets))
                return dataSets[0].GetColumnNames().Count;
            return result;
        }
        private int GetMaxColumnSize(IList<IDataSet> dataSets)
        {
            var maxLength = 0;
            if (IsNotEmpty(dataSets))
            {
                var columnNames = dataSets[0].GetColumnNames();
                maxLength = Math.Max(GetMaxColumnNameLength(columnNames), GetMaxColumnValueLength(dataSets));
            }
            return maxLength;
        }
        private int GetMaxColumnNameLength(IList<string> columnNames)
        {
            int maxLength = 0;
            foreach (var columnName in columnNames)
                if (columnName.Length > maxLength)
                    maxLength = columnName.Length;
            return maxLength;
        }
        private int GetMaxColumnValueLength(IList<IDataSet> dataSets)
        {
            int maxLength = 0;
            foreach (var dataSet in dataSets)
            {
                var values = dataSet.GetValues();
                foreach (var value in values)
                    if (value.ToString().Length > maxLength)
                        maxLength = value.ToString().Length;
            }

            return maxLength;
        }
        private int IncrementMaxColumnSize(int maxColumnSize)
        {
            if (IsEven(maxColumnSize))
                maxColumnSize += 2;
            else
                maxColumnSize += 3;
            return maxColumnSize;
        }


        private bool IsEven(int num)
        {
            return num % 2 == 0;
        }
        private bool IsNotEven(int num)
        {
            return !IsEven(num);
        }
        private bool IsLastRow(int rowsCount, int currentRowNumber)
        {
            return currentRowNumber < rowsCount - 1;
        }
        private bool IsNotEmpty(IList<IDataSet> dataSets)
        {
            return dataSets.Count > 0;
        }



        private string GetHorizontalLine(int count)
        {
            string row="";
            for(int i=0;i<count;i++)
                row += ROW_SEPERATOR;
            return row;
        }
        private string GetTopRowBorder(int maxColumnSize, int columnCount)
        {
            string result = EMPTY_STRING;
            result += TOP_LEFT_CORNER;
            for (var j = 1; j < columnCount; j++)
            {
                result += GetHorizontalLine(maxColumnSize);

                result += T_FIGURE;
            }
            result += GetHorizontalLine(maxColumnSize);
            result += TOP_RIGHT_CORNER + NEW_LINE;
            return result;
        }
        private string GetSectionSeperator(int maxColumnSize, int columnCount)
        {
            string result = LEFT_ROTATE_T_FIGURE;
            for (var j = 1; j < columnCount; j++)
            {
                result += GetHorizontalLine(maxColumnSize);
                result += CROSS_FIGURE;
            }
            result += GetHorizontalLine(maxColumnSize);

            result += RIGHT_ROTATE_T_FIGURE + NEW_LINE;
            return result;
        }
        private string GetBottomRowBorder(int maxColumnSize, int columnCount)
        {
            string result = EMPTY_STRING;
            result += BOTTOM_LEFT_CORNER;
            for (var j = 1; j < columnCount; j++)
            {
                result += GetHorizontalLine(maxColumnSize);
                result += INVERTED_T_FIGURE;
            }
            result += GetHorizontalLine(maxColumnSize);
            result += BOTTOM_RIGHT_CORNER + NEW_LINE;
            return result;
        }
    }

}