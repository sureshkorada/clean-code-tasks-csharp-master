﻿using System;
using System.Runtime.Serialization;

namespace Functions.Task3
{
    [Serializable]
    internal class LoginFailureException : Exception
    {
        public LoginFailureException()
        {
        }

        public LoginFailureException(string message) : base(message)
        {
        }

        public LoginFailureException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected LoginFailureException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}