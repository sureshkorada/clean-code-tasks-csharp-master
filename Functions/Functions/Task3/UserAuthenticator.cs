﻿using Functions.Task3.ThirdParty;

namespace Functions.Task3
{
    public abstract class UserAuthenticator : IUserService
    {
        private readonly ISessionManager sessionManager;

        protected UserAuthenticator(ISessionManager sessionManager)
        {
            this.sessionManager = sessionManager;
        }

        public void Login(string userName, string password)
        {
            LoginUser(GetUserByName(userName), password);
        }

        private void LoginUser(IUser user, string password)
        {

            if (IsPasswordCorrect(user, password))
            {
                sessionManager.SetCurrentUser(user);
            }
            else
            {
                throw new LoginFailureException("Invalid Credentials");
            }
        }

        public abstract bool IsPasswordCorrect(IUser user, string password);
        public abstract IUser GetUserByName(string userName);
    }
}