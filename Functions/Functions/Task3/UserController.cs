﻿using System;
using Functions.Task3.ThirdParty;

namespace Functions.Task3
{
    public abstract class UserController : IController
    {
        private readonly UserAuthenticator userAuthenticator;

        protected UserController(UserAuthenticator userAuthenticator)
        {
            this.userAuthenticator = userAuthenticator;
        }

        public void AuthenticateUser(string userName, string password)
        {
            try
            {
                userAuthenticator.Login(userName, password);
                GenerateSuccessLoginResponse(userName);
            }
            catch (LoginFailureException)
            {
                GenerateFailLoginResponse();
            }
            
        }


        public abstract void GenerateSuccessLoginResponse(string user);
        public abstract void GenerateFailLoginResponse();
    }
}