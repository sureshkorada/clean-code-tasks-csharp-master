﻿using Functions.Task1.ThirdParty;
using System;
using System.Collections.Generic;

namespace Functions.Task1
{
    public class RegisterAccountAction
    {
        public IPasswordChecker PasswordChecker { get; set; }
        public IAccountManager AccountManager { get; set; }

        public void Register(IAccount account)
        {
            ValidateAccountName(account.GetName());
            ValidatePassword(account.GetPassword());
            SetAccountCreatedDate(account);
            SetAddressDetails(account);
            AccountManager.CreateNewAccount(account);
        }

        private void SetAddressDetails(IAccount account)
        {
            account.SetAddresses(GetAddressDetails(account));
        }

        private void SetAccountCreatedDate(IAccount account)
        {
            account.SetCreatedDate(new DateTime());
        }

        private static List<IAddress> GetAddressDetails(IAccount account)
        {
            return new List<IAddress>
            {
                account.GetHomeAddress(),
                account.GetWorkAddress(),
                account.GetAdditionalAddress()
            };
        }

        private void ValidatePassword(string password)
        {
            if (password.Length <= 8)
            {
                if (PasswordChecker.Validate(password) != CheckStatus.Ok)
                {
                    throw new WrongPasswordException();
                }
            }
        }

        private void ValidateAccountName(String accountName)
        {
            if (accountName.Length <= 5)
            {
                throw new WrongAccountNameException();
            }
            
        }
    }
}