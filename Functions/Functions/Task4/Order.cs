﻿using System;
using System.Collections.Generic;
using System.Linq;
using Functions.Task4.ThirdParty;

namespace Functions.Task4
{
    public class Order
    {
        public IList<IProduct> Products { get; set; }

        public double GetPriceOfAvailableProducts()
        {
            RemoveUnavailableProducts();
            return ComputeOrderPrice();
        }

        private double ComputeOrderPrice()
        {
            var orderPrice = 0.0;
            foreach (IProduct p in Products)
            {
                orderPrice += p.GetProductPrice();
            }
            return orderPrice;
        }

        private void RemoveUnavailableProducts()
        {
            IEnumerator<IProduct> enumerator = Products.ToList().GetEnumerator();
            while (enumerator.MoveNext())
            {
                IProduct p1 = enumerator.Current;
                if (IsProductNotAvailable(p1))
                {
                    Products.Remove(p1);
                }
            }
        }

        private bool IsProductNotAvailable(IProduct p1)
        {
            return !p1.IsAvailable();
        }
    }
}
